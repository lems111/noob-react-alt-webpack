require('normalize.css/normalize.css');
require('styles/App.css');

import runBootstrapCustomJS from 'styles/custom'
//import {Navbar, Nav, NavItem} from 'react-bootstrap/lib';
import React from 'react';

class MainLayout extends React.Component {
  static propTypes = {};
  static defaultProps = {};
  componentDidMount() {
    runBootstrapCustomJS();
  }
  componentDidUpdate() {
    runBootstrapCustomJS();
  }

  render() {
    return (
      <div className="row" id="app">
        <nav id="mainNav" className="navbar navbar-default navbar-custom navbar-fixed-top">
          <div className="container">
            <div className="navbar-header page-scroll">
              <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span className="sr-only">Toggle navigation</span>
                Menu
                <i className="fa fa-bars"></i>
              </button>
              <a className="navbar-brand page-scroll" href="#root">My grooming company</a>
            </div>
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul className="nav navbar-nav navbar-right">
                <li className="hidden">
                  <a href="#root"></a>
                </li>
                <li >
                  <a className="page-scroll" href="#services">What we offer</a>
                </li>
                <li>
                  <a className="page-scroll" href="#contact">Contact</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <main>
          {this.props.children}
        </main>
      </div>
    );
  }
}

export default MainLayout;

import React from 'react';

export default function ContactSection() {

  return (
    <footer>
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
            <ul className="list-inline social-buttons">
              <li>
                <a href="#">
                  <i className="fa fa-twitter"></i>
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-facebook"></i>
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <br/>
        <div className="row container">
          <div className="col-md-4 col-md-offset-4">
            <a href="https://www.linkedin.com/in/luis-miranda-945ba7126" target="_blank">designed by aMobile Solutions, LLC</a>
          </div>
        </div>
      </div>
    </footer>
  )
}

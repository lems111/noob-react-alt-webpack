import 'core-js/fn/object/assign';
import React from 'react';
import { Router, Route, browserHistory } from 'react-router';

// layouts
import MainLayout from './app-layout/MainLayout';

// pages
import Home from './components/Main';

export default (
  <Router history={browserHistory}>
    <Route component={MainLayout}>
      <Route path="/" component={Home} />
    </Route>
  </Router>
);

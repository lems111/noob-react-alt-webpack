import React from 'react';

export default function ContactSection() {

  return (
    <section id="contact">
      <div className="container">
        <div className="row">
                <div className="col-lg-12 text-center">
                    <h2 className="section-heading">Contact us</h2>
                </div>
            </div>
        <div className="row text-center">
          <div className="col-md-4 col-md-offset-2">
            <span className="fa-stack fa-4x">
              <i className="fa fa-circle fa-stack-2x text-primary"></i>
              <i className="fa fa-phone fa-stack-1x fa-inverse"></i>
            </span>
            <p className="text-muted">Phone: 222-222-222</p>
          </div>
          <div className="col-md-4">
            <span className="fa-stack fa-4x">
              <i className="fa fa-circle fa-stack-2x text-primary"></i>
              <i className="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
            </span>
            <p className="text-muted">Email: grooming@company.com</p>
          </div>
        </div>
      </div>
    </section>
  );
}

import React from 'react';
import WelcomeSection from './WelcomeSection';
import ServiceSection from './ServiceSection';
import ContactSection from './ContactSection';
import FooterSection from './FooterSection';

export default function Home() {
    return (
      <div>
        <WelcomeSection/>
        <ServiceSection/>
        <ContactSection/>
        <FooterSection/>
      </div>
    );
}
